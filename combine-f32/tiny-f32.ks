%packages

# @admin-tools
# @base-x
# @core
# @dial-up
# @fonts
# @guest-desktop-agents
# @hardware-support
# @input-methods
# @libreoffice
# @multimedia
# @networkmanager-submodules
# @printing
@standard
# @workstation-product

# @firefox
mc

@mate-desktop
# @mate-applications

cpio
genisoimage
nmap-ncat
lua
openssh
openssh-clients
python3-rangehttpserver

# python-unversioned-command
# wpa_supplicant

# pungi requires
slick-greeter-cinnamon

-kde-i18n-*
-kde-l10n-*

-man-pages-*

kernel*

@anaconda-tools

%end
