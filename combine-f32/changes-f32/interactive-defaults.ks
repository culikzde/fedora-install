# Kickstart defaults file for an interative install.
# This is not loaded if a kickstart file is provided on the command line.

keyboard --vckeymap=us --xlayouts='us'

lang en_US.UTF-8

timezone Europe/Prague

selinux --disabled

firstboot --disable

services --disabled=sendmail,ssh,gdm,lightdm,sddm # --enabled=ntpd

network --bootproto=dhcp --onboot=on

%packages

@^mate-desktop-environment
@firefox
mc

%end

%post

echo "DESKTOP=MATE" > /etc/sysconfig/desktop

%end
