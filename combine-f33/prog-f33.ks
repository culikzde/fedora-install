
%packages

@admin-tools
@base-x
@core
@dial-up
@fonts
@guest-desktop-agents
@hardware-support
@input-methods
@libreoffice
@multimedia
@networkmanager-submodules
@printing
@standard
# @workstation-product

@firefox
mc

meld
k3b

# @gnome-desktop
# @epiphany
# gedit
gimp
gnumeric
evolution

@mate-desktop
@mate-applications
# @compiz
# @3d-printing
iptables
firewalld
ntpdate

# @kde-desktop
# @kde-apps
# @kde-education
# @kde-media
# @kde-office
# @kde-telepathy
# kexi

@c-development
@development-libs
@development-tools

gcc
gcc-c++

ccache

automake
libtool

binutils-devel
libtool-ltdl-devel

pkgconfig

# libxml-devel
libxml2-devel

gtk+-devel
gtk-doc

gtk2-devel
gtk2-devel-docs

gtk3-devel
gobject-introspection-devel
gtk3-devel-docs

qt3-devel
qt3-devel-docs
qt3-*

qt-devel
qt-devel-private
qt-doc
qt-*

qt5-qtbase
qt5-qtbase-devel
qt5-qtbase-doc
qt5-qtbase-gui
# qt5-devel
qt5-*

Inventor-devel
InventorXt-devel
Inventor-data
Inventor-demos
Inventor-examples
# Coin2-devel
# Coin3-devel
# SoQt-devel
mesa-demos

# java-1.8.0-openjdk-devel
# java-11-openjdk-devel
# antlr-tool
# antlr-C++
# antlr
# antlr32
# antlr4
# antlrworks

lout

gcc-plugin-devel
# gcc-python2-plugin
# gcc-python3-plugin
graphviz

llvm-devel
clang-devel
clang-analyzer

# PyQt4
# PyQt4-devel
# PyQt4-doc
# PyQt4-webkit

# python2-qt5
# python2-qt5-devel
# python2-qt5-webkit
# python2-qt5-webengine

python3-PyQt4
python3-PyQt4-devel
python3-PyQt4-webkit

python3-qt5
python3-qt5-devel
python3-qt5-webkit
python3-qt5-webengine

qt-qdbusviewer
qt5-qdbusviewer

# gnome-tweak-tool
# gconf-editor
# dconf-editor
glade2
glade

# KDevelop 3
qt3-devel
kdelibs3-devel
flex
flex-devel
libdb-devel
subversion-devel
doxygen

# KDevelop 4
make
cmake
gdb
patch
wget

kate
kate-plugins
libkate
libkate-devel
libkate-docs

kompare
kompare-devel
kdiff3
kdelibs-devel
kde-workspace-devel
# grantlee-devel
grantlee-qt5-devel
libkomparediff2-devel
kactivities-devel
okteta-devel
qtwebkit-devel
qjson-devel
boost-devel
subversion-devel

kdevelop
kdevelop-devel
kdevelop-php
kdevelop-python

kf5-*

lua-devel
squirrel-devel
perl-devel
php-devel
python2-devel
python3-devel
ruby-devel

qt-creator
qt-creator-doc

lazarus
fpc-doc

anjuta
eric
# gambas3-ide # optional group
# monodevelop # optional group

# qemu-system-x86 # optional group

httpd
mariadb-server
postgresql-server
sqlite
# mongodb # optional group
php
php-mysqlnd
php-pgsql
php-pdo
phpMyAdmin
# phpPgAdmin

# httpd-manual
# postgresql-docs
# sqlite-doc
# php-manual-en
# python2-docs
python3-docs
# ruby-doc


source-highlight
dpkg

wmctrl
libXmu-devel

rpm-build

mate-common
vte-devel
dconf-devel

# pyusb

# python2-cherrypy

# php installation
libmcrypt-devel

# python2-pivy

freecad

qt-config

pygtk2
# python2-gobject
# npm

freeglut-devel
mesa-libGLES-devel

anaconda
python3-blivet
intltool
libarchive-devel
pungi

cpio
genisoimage
nmap-ncat
lua
openssh
openssh-clients
python3-rangehttpserver

python-unversioned-command
wpa_supplicant

# pungi requires
# slick-greeter-cinnamon
# mysql-selinux

-kde-i18n-*
-kde-l10n-*

-man-pages-*

-qt5-qt3d-examples

kernel*

@anaconda-tools

%end
