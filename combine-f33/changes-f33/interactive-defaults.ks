# Kickstart defaults file for an interative install.
# This is not loaded if a kickstart file is provided on the command line.

keyboard --vckeymap=us --xlayouts='us'

lang en_US.UTF-8

timezone Europe/Prague

selinux --disabled

firstboot --disable

services --disabled=sendmail,ssh,gdm,lightdm,sddm # --enabled=ntpd

network --bootproto=dhcp --onboot=on

rootpw --iscrypted $6$0rdRb4fp0x5gXQzx$JAW.tpOwCAydLAWip2eUQtLf0bGr6jj3Wo2x6m.XORUuVrZ1Zfrnx4/uwDf2fdGx6wKPafXuT8pXbQhdwyzsU/

user --name=uzivatel --password=$6$Erp/rIh.KmZxxbyM$m6qx0TPzA6nPW..KIKxq90T1jIJnnHIRLDfoMExThRKwL2HITfuHvEUr92/JgCXenUBWT.d7uwb0jUMqInmR3. --iscrypted

%packages

@^mate-desktop-environment
@firefox
mc

%end

%post

mkdir /root/Fedora32
cd /root/Fedora32

tar czf etc0.tgz /etc
tar czf boot0.tgz /boot
tar czf var0.tgz /var

rpm -qa > packages1
rpm -qa --last > review1

groupadd -g 800 zdenek
useradd  -u 800 -g 800 zdenek

groupadd -g 900 inst
useradd  -u 900 -g 900 inst

echo "" >> /etc/sudoers
echo "zdenek ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "inst   ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

echo "DESKTOP=MATE" > /etc/sysconfig/desktop

sed -i -e 's/enabled=1/enabled=0/' /etc/yum.repos.d/fedora-updates.repo
sed -i -e 's/enabled=1/enabled=0/' /etc/yum.repos.d/fedora-modular.repo
sed -i -e 's/enabled=1/enabled=0/' /etc/yum.repos.d/fedora-updates-modular.repo

%end
