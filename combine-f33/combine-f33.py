#!/usr/bin/env python

from __future__ import print_function

# import sys, os, time
# from stat import *
import os
import shutil
import subprocess
import glob
import re
import xml.etree.ElementTree as ET

import optparse

# --------------------------------------------------------------------------

# releasever = "21"
# basearch = "i386"

# releasever = "22"
# basearch = "x86_64"

# releasever = "23"
# basearch = "x86_64"

# releasever = "24"
# basearch = "x86_64"

# releasever = "25"
# basearch = "i386"
# basearch = "x86_64"

# releasever = "26"
# basearch = "x86_64"

# releasever = "27"
# basearch = "i386"
# basearch = "x86_64"

# releasever = "28"
# basearch = "i386"
# basearch = "x86_64"

# releasever = "29"
# basearch = "i386"
# basearch = "x86_64"

# releasever = "30"
# basearch = "i386"
# basearch = "x86_64"

# releasever = "31"
# basearch = "x86_64"

# releasever = "32"
# basearch = "x86_64"

releasever = "33"
basearch = "x86_64"

# --------------------------------------------------------------------------

# check_patch = True
check_patch = False

config_name = "f" + releasever

# --------------------------------------------------------------------------

download_release = "releases"
download_ver = releasever
download_variant = ""
download_flavor = "Everything"
download_mini_flavor = download_flavor
download_mini_subdir = ""
download_mini_suffix = ""

if releasever == "21" :
   download_mini_flavor = "Server"

if releasever in [ "22", "23" ] :
   download_mini_flavor = "Workstation"

if releasever == "24" :
   download_mini_suffix = "-1.2"

if releasever == "25" :
   download_mini_suffix = "-1.3"

if releasever == "26" :
   download_mini_suffix = "-1.5"

if releasever == "27" :
   download_mini_suffix = "-1.6"

if releasever == "28" :
   download_mini_suffix = "-1.1"

if releasever == "29" :
   download_mini_suffix = "-1.2"

if releasever == "30" :
   download_mini_suffix = "-1.2"

if releasever == "31" :
   download_mini_suffix = "-1.9"

if releasever == "32" :
   download_mini_suffix = "-1.6"

if releasever == "33" :
   # download_release = "releases/test"
   # download_variant = "_Beta"
   # download_mini_flavor = "Server"
   # download_mini_suffix = "-1.1"

   download_release = "development"
   # download_ver = "rawhide"
   download_ver = "33"
   download_mini_suffix = "-20201005.n.0"

# --------------------------------------------------------------------------

repo_base = "http://ftp.linux.cz/pub/linux/fedora/linux/"
# repo_base = "http://mirrors.nic.cz/pub/fedora/linux/"

if releasever < "27" :
   repo_base = "http://archives.fedoraproject.org/pub/archive/fedora/linux/"

repo_source = repo_base + download_release + "/" + download_ver + download_variant + "/" + download_flavor

if releasever >= "27" and basearch == "i386" :
   repo_base = "http://ftp-stud.hs-esslingen.de/pub/fedora-secondary/"
   # repo_base = "http://fr2.rpmfind.net/linux/fedora-secondary/"
   # repo_base = "http://ftp.icm.edu.pl/pub/Linux/dist/fedora-secondary/"
   # repo_base = "http://dl.fedoraproject.org/pub/fedora-secondary/"

repo_mid = repo_base + download_release + "/" + download_ver + download_variant + "/"

if releasever in ["21", "22", "25", "28"] :
   repo_base = "ftp://koala.fjfi.cvut.cz/"
   repo_mid = repo_base + "fedora" + download_ver + "/"
   repo_source = repo_base + "fedora" + download_ver + "/" + download_flavor
   if releasever in ["21", "22" ] :
      download_mini_subdir = "iso"

if releasever >= "24" :
   repo_source = repo_source + "/" + "source/tree"
else :
   repo_source = repo_source + "/" + "source/SRPMS"

repo_spec = repo_mid + download_flavor + "/" + basearch + "/" + "os" + "/"

# --------------------------------------------------------------------------

if download_mini_subdir != "" :
   repo_tmp = repo_mid + download_mini_subdir + "/"
else :
   repo_tmp = repo_mid + download_mini_flavor + "/" + basearch + "/" + "iso" + "/"

release_tmp = download_ver.capitalize()

mini_url = ( repo_tmp +
              "Fedora-" + download_mini_flavor + "-netinst-" + basearch + "-" + release_tmp + download_variant + download_mini_suffix + ".iso" )

# --------------------------------------------------------------------------

repo_lines = [
               # "repo --name=abc --baseurl=file:///fedora32/_download",
               "repo --name=fedora --baseurl=" + repo_spec
             ]
source_repo_lines = [ "repo --name=fedora-source --baseurl=" + repo_source ]
compare_repo_lines = [ ]


repo_lines = repo_lines + [

# "repo --name=fedora --baseurl=http://ftp.linux.cz/pub/linux/fedora/linux/releases/$releasever/Everything/$basearch/os/",
# "repo --name=fedora --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/Everything/$basearch/os/",
# "repo --name=fedora --baseurl=" + repo_base + download_release + "/$releasever/" + download_flavor + "/$basearch/os/",

# "repo --name=fedora-updates --baseurl=http://ftp.linux.cz/pub/linux/fedora/linux/updates/$releasever/$basearch/",
# "repo --name=fedora-updates --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/$basearch-updates/",
# "repo --name=fedora-updates --baseurl=" + repo_base + "updates/testing/$releasever/$basearch/",

# "repo --name=fedora-updates-testing --baseurl=http://ftp.linux.cz/pub/linux/fedora/linux/updates/testing/$releasever/$basearch/",
# "repo --name=fedora-updates-testing --baseurl=" + repo_base + "updates/$releasever/$basearch/",

# "repo --name=fusion-free            --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os",
# "repo --name=fusion-nonfree         --baseurl=http://download1.rpmfusion.org/nonfree/fedora/releases/$releasever/Everything/$basearch/os",
# "repo --name=fusion-free-updates    --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch",
# "repo --name=fusion-nonfree-updates --baseurl=http://download1.rpmfusion.org/nonfree/fedora/updates/$releasever/$basearch",

# "repo --name=fusion-free            --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-free",
# "repo --name=fusion-nonfree         --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-nonfree",
# "repo --name=fusion-free-updates    --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-free-updates",
# "repo --name=fusion-nonfree-updates --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-nonfree-updates",

]

# --------------------------------------------------------------------------

# if releasever == "25" and basearch == "x86_64" :
#    repo_lines = repo_lines + [
#       "repo --name=cuda --baseurl=ftp://koala.fjfi.cvut.cz/cuda-8.0/",
#
#       "repo --name=gcc5 --baseurl=ftp://koala.fjfi.cvut.cz/cuda-gcc5/",
#       # "repo --name=gcc5 --baseurl=http://ftp.scientificlinux.org/linux/scientific/7x/external_products/softwarecollections/$basearch/",
#
#       # "repo --name=fusion-free            --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-free",
#       # "repo --name=fusion-nonfree         --baseurl=file:///mnt/other/fedora$releasever-updates/fusion-nonfree",
#       # "repo --name=fusion-free-updates    --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-free-updates",
#       # "repo --name=fusion-nonfree-updates --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/fusion-nonfree-updates",
#
#       "repo --name=fusion-free            --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os",
#       "repo --name=fusion-nonfree         --baseurl=http://download1.rpmfusion.org/nonfree/fedora/releases/$releasever/Everything/$basearch/os",
#       "repo --name=fusion-free-updates    --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch",
#       "repo --name=fusion-nonfree-updates --baseurl=http://download1.rpmfusion.org/nonfree/fedora/updates/$releasever/$basearch",
#
#       # "repo --name=fusion-nonfree-testing --baseurl=http://download1.rpmfusion.org/nonfree/fedora/updates/testing/25/x86_64",
#    ]

# --------------------------------------------------------------------------

if releasever == "30" :
   repo_lines = repo_lines + [
      "repo --name=creator --baseurl=ftp://koala.fjfi.cvut.cz/fedora$releasever/qt-creator-$basearch/",
   ]

# --------------------------------------------------------------------------

combine_dir = os.getcwd ()

data_dir = combine_dir
# data_dir = os.path.join (combine_dir, "..", "fedora" + download_ver + "-" + basearch)

# --------------------------------------------------------------------------

cache_subdir = "_cache"
mini_subdir = "_mini"
updates_subdir = "_updates"
product_subdir = "_product"

other_subdir = "_other"
large_subdir = "_large"
source_subdir = "_source"

pungi_subdir = "_complete"
compose_subdir = "_compose"
import_subdir = "_import"
export_subdir = "_export"
import_dnf_subdir = "_import_dnf"
export_dnf_subdir = "_export_dnf"
iso_subdir = "_iso"

# --------------------------------------------------------------------------

combine_script = __file__

prog_config = "prog-" + config_name + ".ks"
large_config = "large-" + config_name + ".ks"
source_config = "src-" + config_name + ".ks"
small_config = "small-" + config_name + ".ks"
tiny_config = "tiny-" + config_name + ".ks"

local_config = "local.ks"

changes_subdir = "changes-" + config_name

comps_script = ""
if releasever == "29" :
   comps_script = "comps.sh"

# --------------------------------------------------------------------------

package_tree_dir = os.path.join (data_dir, import_subdir, "Packages")

# --------------------------------------------------------------------------

compare_repo_lines = compare_repo_lines + [
   "repo --name=compare --baseurl=file://" + package_tree_dir + "/..",
]

# --------------------------------------------------------------------------

# https://github.com/rhinstaller/anaconda
# fedora 23: package anaconda-code, /usr/lib64/python3.4/site-packages/pyanaconda

# https://pagure.io/pungi
# fedora 23: package, pungi, /usr/lib/python2.7/site-packages/pungi/gather.py
# add_multilib
# add_package ... lookaside

# --------------------------------------------------------------------------

update_packages = ["mc", "gpm-libs",
                   "cpio",
                   "genisoimage",
                   "libusal", "file-libs",
                   "nmap-ncat", "lua" ]

if releasever > "21" :
   update_packages = update_packages + ["openssh", "openssh-clients"]

if releasever >= "26" :
   update_packages = update_packages + ["slang"]

if releasever >= "28" :
   update_packages = update_packages + ["libssh2"]

if releasever >= "29" :
   update_packages = update_packages + ["rpm"]

if releasever >= "32" :
   update_packages = update_packages + ["python3-rangehttpserver"]

# --------------------------------------------------------------------------

product_packages = ["anaconda-core", "anaconda-gui"]

if releasever <= "22" :
   product_packages = product_packages + ["python-blivet"]
else:
   product_packages = product_packages + ["python3-blivet"]

# --------------------------------------------------------------------------

product_files = [ ]

def initProductFiles () :

    addFile ("/root/.config/mc/hotlist") # !?
    addFile ("/root/.config/mc/mc.ext") # !?
    addFile ("/usr/share/anaconda/interactive-defaults.ks")
    if releasever == "22" :
       addFile ("/usr/lib/systemd/system/anaconda-shell@.service")
       addFile ("/usr/lib/systemd/system/anaconda-shell.service")
       addFile ("/usr/lib/systemd/system/anaconda-direct.service")

    if releasever >= "32" :
       addDiff ("/usr/lib/python3.4/site-packages/blivet/formats/fs.py")
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/welcome.py")
       if releasever == "32" :
          addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/storage/checker.py")
          addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/bootloader/grub2.py")
          addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/installation_source.py")
          addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/payload/__init__.py")
       if releasever >= "33" :
          # addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/modules/storage/checker/checker.py")
          addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/modules/storage/bootloader/grub2.py")
          # addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/modules/payloads/payload/__init__.py")

       #  addFile ("/usr/share/anaconda/ui/spokes/storage.glade")

"""
def initClassicProductFiles () :

    # on i386 : lib64 -> lib
    # on fedora 21 and 22 : python3.4 -> python2.7
    # on fedora 24 and 25 : python3.4 -> python3.5
    # on fedora 26, 27, 28 : python3.4 -> python3.6
    # on fedora 29 : python3.4 -> python3.7
    # on fedora 32 : python3.4 -> python3.8
    # on fedora 33 : python3.4 -> python3.9

    addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/welcome.py")
    if releasever <= "26" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/source.py")
    else :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/installation_source.py")
    addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/storage.py")
    if releasever <= "26" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/custom.py")
    else :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/custom_storage.py")
    addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/lib/summary.py")
    if releasever <= "25" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/user.py")
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/spokes/password.py")
    if releasever >= "26" and releasever <= "27":
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/ui/gui/helpers.py")
    if releasever <= "30" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/kickstart.py")
    addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/product.py")
    if releasever <= "25" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/packaging/__init__.py")
    if releasever >= "26" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/payload/__init__.py")
    if releasever <= "29" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/flags.py")
    if releasever == "21" :
       addDiff ("/usr/lib64/python2.7/site-packages/pyanaconda/install.py")
    if releasever >= "25" and releasever <= "29" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/storage_utils.py")

    if releasever <= "22" :
       addDiff ("/usr/lib/python2.7/site-packages/blivet/formats/fs.py")
    if releasever > "22" :
       addDiff ("/usr/lib/python3.4/site-packages/blivet/formats/fs.py")

    if releasever == "21" :
       addDiff ("/usr/lib/python2.7/site-packages/blivet/__init__.py")

    if releasever == "22" :
       addDiff ("/usr/lib/python2.7/site-packages/blivet/osinstall.py")
    if releasever >= "23" and releasever <= "27" :
       addDiff ("/usr/lib/python3.4/site-packages/blivet/osinstall.py")
    if releasever >= "28" and releasever <= "30" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/storage/osinstall.py")

    if releasever >= "30" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/storage/root.py")
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/storage/checker.py")
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/bootloader/execution.py")
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/bootloader/grub2.py")

    addDiff ("/usr/share/anaconda/ui/spokes/storage.glade")
    addDiff ("/usr/share/anaconda/ui/hubs/summary.glade")
    if releasever <= "26" :
       addDiff ("/usr/share/anaconda/ui/spokes/custom.glade")
       addDiff ("/usr/share/anaconda/ui/spokes/source.glade")
    else :
       addDiff ("/usr/share/anaconda/ui/spokes/custom_storage.glade")
       addDiff ("/usr/share/anaconda/ui/spokes/installation_source.glade")

    if releasever > "21" and releasever <= "30" :
       addDiff ("/usr/lib64/python3.4/site-packages/pyanaconda/image.py")

"""

# fedora 21: rpm python-blivet,  /usr/lib/python2.7
# fedora 22: rpm python-blivet,  /usr/lib/python2.7
# fedora 23: rpm python3-blivet, /usr/lib/python3.4
# fedora 24: rpm python3-blivet, /usr/lib/python3.5

# fedora 21: rpm anaconda-core,  /usr/lib64/python2.7
# fedora 22: rpm anaconda-core,  /usr/lib64/python2.7
# fedora 23: rpm anaconda-core,  /usr/lib64/python3.4
# fedora 24: rpm anaconda-core,  /usr/lib64/python3.5

# --------------------------------------------------------------------------

if releasever <= "21" :
   yum_cmd = "yum"
else:
   yum_cmd = "dnf"

# --------------------------------------------------------------------------

class Options :
    def __init__ (self) :
        self.install = False
        self.mini = False
        self.updates = False
        self.product = False
        self.combine = False
        self.iso = False
        self.all = False
        self.prog = False
        self.prog_src = False
        self.other = False
        self.large = False
        self.large_src = False
        self.source = False
        self.small = False
        self.small_src = False
        self.tiny = False
        self.tiny_src = False
        self.qemu = False
        self.copy = False
        self.no_download = False
        self.no_group = False
        self.no_gpg = False
        self.delta = False

def read_options () :

    parser = optparse.OptionParser ()
    parser.add_option ("--install", dest="install", action="store_true", help="Install necessary packages")
    parser.add_option ("--mini", dest="mini", action="store_true", help="Download netinst iso image")
    parser.add_option ("-u", "--updates", dest="updates", action="store_true", help="Create updates.img")
    parser.add_option ("-p", "--product", dest="product", action="store_true", help="Create product.img")
    parser.add_option ("-c", "--combine", dest="combine", action="store_true", help="Copy files to pungi directory")
    parser.add_option ("--iso",  dest="iso", action="store_true", help="Create iso file")
    parser.add_option ("--all", dest="all", action="store_true", help="Create iso image")

    parser.add_option ("--prog", dest="prog", action="store_true", help="Collect rpm files, " + prog_config + " --> " + pungi_subdir)
    parser.add_option ("--prog-src",  dest="prog_src", action="store_true", help="Collect rpm and srpm files, " + prog_config + " --> " + pungi_subdir)
    parser.add_option ("--other",  dest="other", action="store_true", help="Collect other rpm files, " + large_config + " --> " + other_subdir)
    parser.add_option ("--large",  dest="large", action="store_true", help="Collect rpm files, " + large_config + " --> " + large_subdir)
    parser.add_option ("--large-src",  dest="large_src", action="store_true", help="Collect rpm files, " + large_config + " --> " + large_subdir)
    parser.add_option ("--source", dest="source", action="store_true", help="Collect rpm and srpm files, " + source_config + " --> " + source_subdir)

    parser.add_option ("--tiny",     dest="tiny", action="store_true", help="Tiny rpm files, " + tiny_config + " --> " + pungi_subdir)
    parser.add_option ("--tiny-src", dest="tiny_src", action="store_true", help="Tiny rpm ans srpm files, " + tiny_config + " --> " + pungi_subdir)

    parser.add_option ("--small",     dest="small", action="store_true", help="Small rpm files, " + small_config + " --> " + pungi_subdir)
    parser.add_option ("--small-src", dest="small_src", action="store_true", help="Small rpm and srpm files, " + small_config + " --> " + pungi_subdir)

    parser.add_option ("--no-download", dest="no_download", action="store_true", help="Do not download groups and packages")
    parser.add_option ("--no-group",    dest="no_group", action="store_true", help="Do not download groups")
    parser.add_option ("--no-gpg",      dest="no_gpg", action="store_true", help="Do not check gpg")

    # parser.add_option ("--qemu",  dest="qemu", action="store_true", help="Start QEmu")
    # parser.add_option ("--copy", dest="copy", action="store_true", help="Copy rpm files")
    parser.add_option ("--delta", dest="delta", action="store_true", help="Compare product and original files")

    global opt
    global arg

    opt = Options ()

    (opt, arg) = parser.parse_args (values=opt)

    if len (arg) > 0 :
       parser.error ("too many arguments")

# --------------------------------------------------------------------------

# find . -name '*.rpm' -printf "%f\n" | sort

# unpack: gunzip -dc $ARCHIVE_FILE | cpio -id
# pack:   find . | cpio -c -o  | gzip -9cv > $ARCHIVE_FILE
# list:   gunzip -dc $ARCHIVE_FILE | cpio -ivt

# --------------------------------------------------------------------------

def create_dir (working_dir) :
    if not os.path.isdir (working_dir) :
       os.mkdir (working_dir)

def enter_dir (working_dir) :
    # print "enter_dir", working_dir
    create_dir (working_dir)
    os.chdir (working_dir)

def enter_subdir (working_dir) :
    enter_dir (os.path.join (data_dir, working_dir))

def create_subdir (working_dir) :
    create_dir (os.path.join (data_dir, working_dir))

def original_dir () :
    os.chdir (combine_dir)

def create_directories (working_dir) :
    if not os.path.isdir (working_dir) :
       os.makedirs (working_dir)

# --------------------------------------------------------------------------

def get_rpm (source_dir, name) :
    files = glob.glob (os.path.join (source_dir, name + "-[0-9]*.rpm"))
    if len (files) > 1 :
       files = glob.glob (os.path.join (source_dir, name + "-[0-9]*." + basearch + ".rpm"))
       if len (files) == 0 :
          files = glob.glob (os.path.join (source_dir, name + "-[0-9]*.noarch.rpm"))
    # print files
    if len (files) == 1 :
       return files [0]
    elif len (files) > 1 :
       print ("Too many packages", files)
       # raise Exception ("Too many packages " + name)
       return ""
    else :
       letter = name [0].lower ()
       letter = ""
       files = glob.glob (os.path.join (source_dir, letter, name + "-[0-9]*.rpm"))
       if len (files) == 1 :
          return files [0]
       elif len (files) > 1 :
          print ("Too many packages", files)
          # raise Exception ("Too many packages " + name)
          return ""
       else :
          print ("Missing package", name)
          # raise Exception ("Missing package " + name)
          return ""

def copy_rpm (source_dir, name, target_dir = "") :
    ok = True
    src_file = get_rpm (source_dir, name)
    if src_file == "" :
       ok = False
    else :
       dst_file = os.path.basename (src_file)
       if target_dir != "" :
          dst_file = os.path.join (target_dir, fst_file)
       if not os.path.isfile (dst_file) :
          shutil.copyfile (src_file, dst_file)
    return ok

def unpack_rpm (inp_name) :
    # os.system ("rpm2cpio " + inp_name + " | cpio -id")
    proc1 = subprocess.Popen (["rpm2cpio", inp_name], stdout=subprocess.PIPE)
    subprocess.check_call (["cpio", "-id"], stdin=proc1.stdout)
    proc1.communicate ()

# --------------------------------------------------------------------------

environments = ["mate-desktop-environment", "custom-environment" ]
groups = [ ]
packages = [ ]

def read_packages (fileName) :
    global groups
    global packages

    groups = []
    packages = []

    f = open (fileName, "r")
    for line in f :
        text = line.strip ()
        if not text.startswith ('#') :
           if not text.startswith ('%') :
              if not text.startswith ('-') :
                 for name in text.split () :
                     if name.startswith ('^@') :
                        environments.append (name[2:])
                     elif name.startswith ('@') :
                        groups.append (name[1:])
                     else :
                        packages.append (name)
    f.close ()

    if 0 :
       for group in groups :
          print ("GRPOUP", group)
       for package in packages :
          print ("PACKAGE", package)

# --------------------------------------------------------------------------

def download_packages (working_subdir, dnf_subdir, from_repo) :
    enter_subdir (dnf_subdir)
    create_dir ("etc")
    create_dir ("etc/yum.repos.d") # empty directory, prevent using global /etc/yum.repos.d
    original_dir ()

    install_root = os.path.abspath (dnf_subdir)
    download_dir = os.path.abspath (os.path.join (working_subdir, "Packages"))

    enter_subdir (working_subdir)

    keys = glob.glob ("/etc/pki/rpm-gpg/*")
    subprocess.check_call (["rpmkeys",
                            "--root=" + install_root,
                            "--import" ] + keys )


    add_opt = [ ]
    if opt.no_gpg :
       add_opt = [ "--nogpgcheck" ]

    if len (groups) > 0 and not opt.no_group:
       print ()
       print ("Download groups")

       subprocess.check_call (["dnf", "group", "install",
                               # "--verbose",
                               "--assumeyes",
                               # "--nogpgcheck",
                               "--releasever=" + releasever,
                               # "--forcearch=" + basearch,
                               "--repofrompath", "abc," + from_repo,
                               "--downloadonly",
                               "--destdir=" + download_dir,
                               "--installroot=" + install_root ] +
                              add_opt +
                              [ group for group in groups ])

    if len (packages) > 0 :
       print ()
       print ("Download packages")

       subprocess.check_call (["dnf", "download",
                               # "--verbose",
                               # "--assumeyes",
                               "--resolve", # resolve dependencies
                               "--releasever=" + releasever,
                               "--arch=" + basearch,
                               "--arch=noarch" ,
                               "--repofrompath", "abc," + from_repo,
                               "--destdir=" + download_dir,
                               "--installroot=" + install_root ] +
                              add_opt +
                              [ package for package in packages ])

    original_dir ()

# --------------------------------------------------------------------------

def http_to_rsync (s) :
    return s.replace ("http://", "", 1).replace ("/", "::", 1)

def refresh (src) :
    print ()
    print ("Refresh package time")
    cmd = "rsync -v -t --size-only --existing " + src + "/* ." # slash and dot are important
    print (cmd)
    subprocess.check_call (cmd, shell=True)

def refresh_subdirs (src) :
    print ()
    print ("Refresh package time")
    for name in "0123456789abcdefghijklmnopqrst" :
        cmd = "rsync -v -t --size-only --existing " + src + "/" + name + "/* ." # slash and dot are important
        print (cmd)
        if name <= "9" :
           subprocess.call (cmd, shell=True)
        else :
           subprocess.check_call (cmd, shell=True)

# --------------------------------------------------------------------------

def copy_comps_file (dnf_subdir, dst_file) :
    pattern = os.path.join (data_dir, dnf_subdir, "var/cache/dnf", "**", "*comps*.xml")
    # print ("FIND", pattern)
    comps_files = glob.glob (pattern, recursive=True)
    if len (comps_files) == 0 :
       raise Exception ("Missing comps file")
    elif len (comps_files) > 1 :
       raise Exception ("Too many comps files")
    else :
       src_file = comps_files [0]
       print ()
       print ("Copying", dst_file, "from ", dnf_subdir)
       shutil.copyfile (src_file, dst_file)

def clean_items (branch, tag) :
    for elem in branch.findall (tag) :
        with_lang = False
        for attr in elem.attrib :
            if attr.endswith ("}lang") :
               with_lang = True
        if with_lang :
           # print ("REMOVE", elem , "FROM", branch)
           branch.remove (elem)

def clean_xml_lang (root, tag) :
    for elem in root.findall (tag) :
       clean_items (elem, "name")
       clean_items (elem, "description")

def clean_requires (root) :
    for branch1 in root.findall ("group") :
        for branch2 in branch1.findall ("packagelist") :
            for elem in branch1.findall ("packagereq") :
                # if "requires" in elem.attrib :
                #     if elem.attrib ["requires"]  == "cinnamon" :
                       if "type" in elem.attrib :
                           if elem.attrib ["type"]  == "conditional" :
                              branch2.remove (elem)

def modify_comps_file (comps_file) :

    tree = ET.parse (comps_file)
    root = tree.getroot ()

    for elem in root.findall ("group") :
        name = elem.find ("id").text
        # print ("GROUP", name)
        if name not in groups :
           root.remove (elem)

    for elem in root.findall ("environment") :
        name = elem.find ("id").text
        if name not in environments :
           root.remove (elem)

    for elem in root.findall ("category") :
        root.remove (elem)

    clean_xml_lang (root, "group")
    clean_xml_lang (root, "environment")
    clean_xml_lang (root, "category")

    # clean_requires (root)

    tree.write (comps_file )
    # subprocess.call (["comps_filter", "--remove-translations", "--arch", basearch, comps_file])

# --------------------------------------------------------------------------

def create_repo (comps_file = "") :
    print ()
    print ("Create repo in", os.getcwd ())
    add_opt = []
    if comps_file != "" :
       add_opt = ["-g", comps_file]
    subprocess.check_call (["createrepo", "."] + add_opt)

# --------------------------------------------------------------------------

def purge_directory (dst_dir) :
    if os.path.isdir (dst_dir) :
       shutil.rmtree  (dst_dir)

def move_directory (src_dir, subdir, dst_dir) :
    src_dir = os.path.join (src_dir, subdir)
    dst_dir = os.path.join (dst_dir, subdir)
    if os.path.isdir (src_dir) :
       purge_directory (dst_dir)
       print ("move", src_dir, "to", dst_dir)
       os.rename (src_dir, dst_dir)

def move_repository (src_dir, dst_dir) :
    move_directory (src_dir, "Packages", dst_dir)
    move_directory (src_dir, "repodata", dst_dir)

def copy_info (src_dir, file_name, dst_dir) :
    print ("copying ", file_name)
    shutil.copyfile (os.path.join (src_dir, file_name), os.path.join (dst_dir, file_name))

def copy_treeinfo (src_dir, dst_dir) :
    copy_info (src_dir, ".discinfo", dst_dir)
    copy_info (src_dir, ".treeinfo", dst_dir)
    copy_info (src_dir, "media.repo", dst_dir)

# --------------------------------------------------------------------------

def proc_variant (variant_name, config_file, working_subdir, src = False) :

    "read environments, groups and packages"
    read_packages (config_file)

    "download to import_subdir"
    if not opt.no_download:
       download_packages (import_subdir, import_dnf_subdir, repo_spec)

       enter_subdir (os.path.join (import_subdir, "Packages"))
       refresh_subdirs (http_to_rsync (repo_spec + "Packages"))
       original_dir ()

       enter_subdir (import_subdir)
       comps_file = "comps.xml"
       copy_comps_file (import_dnf_subdir, comps_file)
       create_repo (comps_file)
       original_dir ()

    "local copy to export_subdir"
    print ()
    local_spec = "file://" + os.path.join (data_dir, import_subdir)
    download_packages (export_subdir, export_dnf_subdir, local_spec)

    enter_subdir (os.path.join (export_subdir, "Packages"))
    refresh (os.path.join (data_dir, import_subdir, "Packages"))
    original_dir ()

    enter_subdir (export_subdir)

    "move packages to subdirectories"
    os.chdir ("Packages")
    names = os.listdir (".")
    names.sort ()
    for name in names :
        if name.endswith (".rpm") and os.path.isfile (name) :
           letter = name [0].lower ()
           subdirs = { }
           if letter >= '0' and letter <= '9' or letter >= 'a' and letter <= 'z' :
              if letter not in subdirs :
                 if not os.path.isdir (letter) :
                    os.mkdir (letter)
                    subdirs [letter] = True
              os.rename (name, os.path.join (letter, name))
    os.chdir ("..")

    "create repository with comps.xml"
    comps_file = "comps.xml"
    copy_comps_file (import_dnf_subdir, comps_file)
    modify_comps_file (comps_file)
    create_repo (comps_file)

    original_dir ()

    "move repository to working_subdir"
    print ()
    create_subdir (working_subdir)
    move_repository (export_subdir, working_subdir)
    copy_treeinfo (mini_subdir, working_subdir)

# --------------------------------------------------------------------------

def proc_other () :
    repo_lines = repo_lines + compare_repo_lines

    orig_rpms = [ ]
    orig_files = findFiles (pungi_subdir)
    for rel_name in orig_files :
        file_name = os.path.basename (rel_name)
        name, ext = os.path.splitext (file_name)
        if ext == ".rpm" :
           orig_rpms.append (file_name)

    enter_subdir (other_subdir)
    config_with_repo (large_config)
    gather_lookaside ()

    new_files = findFiles (".")
    for rel_name in new_files :
        file_name = os.path.basename (rel_name)
        if file_name in orig_rpms :
           print ("remove ", rel_name)
           os.remove (rel_name)

    original_dir ()

# --------------------------------------------------------------------------

def proc_mini () :
    enter_subdir (mini_subdir)

    info_url = os.path.dirname (mini_url)
    info_url = os.path.join (info_url, "..", "os")
    for name in [".discinfo", ".treeinfo", "media.repo"] :
        print (os.path.join (info_url, name), "->", name)
        subprocess.call (["curl", os.path.join (info_url, name), "-o", name])

    iso_file = os.path.basename (mini_url)
    print (mini_url , "->", iso_file)
    if not os.path.isfile (iso_file) :
       subprocess.call (["curl", mini_url, "-o", iso_file])

# --------------------------------------------------------------------------

def proc_iso () :
    enter_subdir (iso_subdir)
    subprocess.check_call (["genisoimage",
                            "-untranslated-filenames",
                            "-J", "-joliet-long", "-rational-rock", "-translation-table",
                            "-input-charset", "utf-8",
                            # "-graft-points", "-path-list", "fedora-dvd.iso-graft-points",
                            "-b", "isolinux/isolinux.bin",
                            "-c", "isolinux/boot.cat",
                            "-no-emul-boot",
                            "-boot-load-size", "4",
                            "-boot-info-table",
                            "-volid", "Fedora-" + releasever + "-" + basearch,
                            "-o",  "fedora-" + releasever + ".iso",
                            os.path.join (data_dir, pungi_subdir),
                           ])
    original_dir ()

# --------------------------------------------------------------------------

def proc_install () :
    subprocess.call ([yum_cmd, "install", "patch"])

# --------------------------------------------------------------------------

def proc_qemu () :
    enter_subdir (pungi_subdir)

    # iso_file = "Fedora-" + flavor + "-DVD-" + basearch + "-" + download_ver + ".iso"
    # iso_file = os.path.join (releasever, flavor, basearch, "iso", iso_file)

    iso_file = "Fedora-DVD-" + basearch + "-" + download_ver + ".iso"
    iso_file = os.path.join (releasever, basearch, "iso", iso_file)

    subprocess.call ("qemu-system-" + basearch + " -cdrom " + iso_file, shell=True)

    original_dir ()

# --------------------------------------------------------------------------

def copy_file (src_file, dst_file) :
    # dst_file = os.path.join (dst_dir, os.path.basename (src_file))
    print ("copy", src_file, "->", dst_file)
    shutil.copyfile (src_file, dst_file)

def proc_combine () :
    enter_subdir (mini_subdir)
    iso_file = os.path.basename (mini_url)

    target_dir = os.path.join (data_dir, pungi_subdir)

    purge_directory (os.path.join (target_dir, "_build"))
    purge_directory (os.path.join (target_dir, "images"))
    purge_directory (os.path.join (target_dir, "isolinux"))

    iso_files = subprocess.check_output ("isoinfo -i" + iso_file + " -R -f", shell=True)
    for file_name_bytes in iso_files.splitlines () :
        file_name = str (file_name_bytes, "ascii")
        print ("FILE", file_name)
        if file_name.startswith ("/images/") or file_name.startswith ("/isolinux/") or file_name.startswith ("/LiveOS/") :
           if not file_name.startswith ("/images/pxeboot") and not file_name.endswith ("/TRANS.TBL"):
              target_file =  target_dir + "/" + file_name # no join, file_name starts with "/"
              print ("unpack", file_name, "->", target_file)
              create_directories (os.path.dirname (target_file))
              if not os.path.isfile (target_file) :
                 subprocess.call ("isoinfo -i" + iso_file + " -R -x " + file_name + " > " + target_file , shell=True)

    # change DVD label in isolinux.cfg
    isolinux_cfg = target_dir + "/isolinux/isolinux.cfg"
    new_label = "Fedora-" + download_ver + "-" + basearch
    subprocess.check_call ("sed -i -e 's/LABEL=\\S*/LABEL=" + new_label + "/' " + isolinux_cfg, shell=True)

    copy_file (os.path.join (data_dir, updates_subdir, "updates.img"), os.path.join (target_dir, "images", "updates.img"))
    copy_file (os.path.join (data_dir, product_subdir, "product.img"), os.path.join (target_dir, "images", "product.img"))

    target_dir = os.path.join (target_dir, "_build")
    create_dir (target_dir)

    copy_file (os.path.join (data_dir, combine_script), os.path.join (target_dir, combine_script))
    copy_file (os.path.join (data_dir, prog_config), os.path.join (target_dir, prog_config))
    if comps_script != "" :
       copy_file (os.path.join (data_dir, comps_script), os.path.join (target_dir, comps_script))

    create_dir (os.path.join (target_dir, changes_subdir))
    for p in product_files :
       copy_file (os.path.join (data_dir, changes_subdir, p.input_name), os.path.join (target_dir, changes_subdir, p.input_name))

    original_dir ()

# -------------------------------------------------------------------------

def proc_copy () :

    file_list = "packages-" + config_name + "-" + basearch + ".txt"
    packages_subdir = "_packages"

    create_dir (packages_subdir)

    f = open (file_list, "r")
    for file_name in f :
        file_name = file_name.rstrip('\n').rstrip('\r')
        file_name = re.sub ("#.*", "", file_name)
        file_name = file_name.strip()
        if file_name != "" :
           letter = file_name [0].lower ()
           letter = ""
           src_file = os.path.join (package_tree_dir, letter, file_name)
           dst_file = os.path.join (packages_subdir, letter, file_name)
           dst_dir = os.path.join (packages_subdir, letter)
           if os.path.isfile (src_file) : # !?
              create_dir (dst_dir)
              if not os.path.isfile (dst_file) : # !?
                 shutil.copyfile (src_file, dst_file)
           else :
              print ("missing", file_name)

# --------------------------------------------------------------------------

def copy_packages (packages, source_dir) :
    stop = False
    for p in packages :
       print ("copying package", source_dir, p)
       ok = copy_rpm (source_dir, p)
       if not ok :
          stop = True
    if stop :
      raise Exception ("Error when copying packages")


def unpack_packages (packages, package_dir) :
    for p in packages :
       print ("unpacking package", p)
       file_name = get_rpm (package_dir, p)
       if file_name == "" :
          raise Exception ("Unknown package " + p)
       else :
          file_name = os.path.basename (file_name)
          print ("unpacking file", file_name)
          inp_name = os.path.join (package_dir, file_name)
          unpack_rpm (inp_name)

# --------------------------------------------------------------------------

def proc_updates () :

    purge_directory (updates_subdir)
    enter_subdir (updates_subdir)

    enter_dir ("packages")
    copy_packages (update_packages, package_tree_dir)
    os.chdir ("..")

    enter_dir ("unpacked")
    unpack_packages (update_packages, "../packages")
    if releasever > "21" :
       subprocess.check_call ("find . -type f -exec chmod -r {} \\;", shell=True)

    print ("packing updates.img")
    # os.system ("tar czf ../updates.img *")
    subprocess.check_call ("tar czf ../updates.img *", shell=True)
    original_dir ()

# --------------------------------------------------------------------------

class Product :
    def __init__ (self) :
       self.full_name = ""
       self.local_name = ""
       self.diff = False

def getLocalName (name) :
    local_name = os.path.basename (name)
    if local_name == "__init__.py" :
       local_name = os.path.basename (os.path.dirname (name)) + "-init.py"
    return local_name

def addFile (name, diff = False) :
    if name.startswith ("/") :
       name = name [1:]

    if basearch == "i386" :
       name = re.sub ("/lib64/", "/lib/", name)

    if releasever <= "22" : # compare strings
       name = re.sub ("/python3.4/", "/python2.7/", name)

    if releasever == "24" or releasever == "25" :
       name = re.sub ("/python3.4/", "/python3.5/", name)

    if releasever >= "26" and releasever <= "28" :
       name = re.sub ("/python3.4/", "/python3.6/", name)

    if releasever >= "29" and releasever <= "31" :
       name = re.sub ("/python3.4/", "/python3.7/", name)

    if releasever == "32" :
       name = re.sub ("/python3.4/", "/python3.8/", name)

    if releasever >= "33" :
       name = re.sub ("/python3.4/", "/python3.9/", name)

    local_name = getLocalName (name)

    for p in product_files :
       if p.local_name == local_name :
          raise Exception ("Duplicated local name " + local_name)

    p = Product ()
    p.full_name = name
    p.local_name = local_name
    p.diff = diff

    p.input_name = p.local_name
    if p.diff :
       p.input_name = p.input_name + ".diff"

    product_files.append (p)

def addDiff (name) :
    addFile (name, True)

# --------------------------------------------------------------------------

def proc_product () :

    purge_directory (product_subdir)
    enter_subdir (product_subdir)

    enter_dir ("packages")
    copy_packages (product_packages, package_tree_dir)
    os.chdir ("..")

    enter_dir ("unpacked")
    unpack_packages (product_packages, "../packages")
    os.chdir ("..")

    enter_dir ("tree")

    used_files = [ ]

    for p in product_files :

       rel_file = os.path.join (".",  p.full_name)
       rel_dir = os.path.dirname (rel_file)
       create_directories (rel_dir)

       if not p.diff :
          print ("copying file", rel_file)
          shutil.copyfile (os.path.join (combine_dir, changes_subdir, p.input_name), rel_file)
       else :
          shutil.copyfile (os.path.join ("../unpacked", p.full_name), rel_file)
          params = ["patch", rel_file, os.path.join (combine_dir, changes_subdir, p.input_name)]
          if check_patch :
             subprocess.check_call (params)
          else :
             subprocess.call (params)

       used_files.append (p.input_name)

    print ()
    subitems = os.listdir (os.path.join (combine_dir, changes_subdir))
    for s in subitems :
       if s not in used_files :
          print ("UNUSED FILE", s)

    print ()
    subprocess.call ("find . -name '*.rej'", shell=True)
    print ()

    print ("packing product.img")
    # os.system ("tar czf ../product.img *")
    subprocess.check_call ("tar czf ../product.img --exclude='*.orig' *", shell=True)
    # subprocess.check_call ("tar czf ../product.img *", shell=True)
    original_dir ()

# --------------------------------------------------------------------------

def findFiles (path, rel_path="") :

    answer = [ ]
    subitems = os.listdir (os.path.join (path, rel_path))
    subitems.sort ()
    for name in subitems :
       rel_name = os.path.join (rel_path, name)
       long_name = os.path.join (path, rel_name)
       if os.path.isfile (long_name) :
          answer.append (rel_name)
       elif os.path.isdir (long_name) :
          answer = answer + findFiles (path, rel_name)
    return answer

# --------------------------------------------------------------------------

def proc_delta () :

    enter_subdir (product_subdir)
    create_dir ("tree-delta")

    files = findFiles ("tree")
    for file_name in files :
        if not file_name.endswith (".orig") :
           first_file = os.path.join ("unpacked", file_name)

           second_file = os.path.join ("tree", file_name)

           local_name = getLocalName (file_name)
           target_file = os.path.join ("tree-delta", local_name)

           if os.path.isfile (first_file) :
              print (file_name + ".diff")
              target_file = target_file + ".diff"
              subprocess.call ("diff -U 5 -r " + first_file + " " + second_file + " > " + target_file, shell=True)
           else :
              print (file_name)
              shutil.copyfile (second_file, target_file)

    original_dir ()

# --------------------------------------------------------------------------

def proc_tiny (src = False) :
    proc_variant ("Tiny", tiny_config, pungi_subdir, src)

def proc_small (src = False) :
    proc_variant ("Small", small_config, pungi_subdir, src)

def proc_prog (src = False) :
    proc_variant ("Prog", prog_config, pungi_subdir, src)

def proc_large (src = False) :
    proc_variant ("Large", large_config, large_subdir, src)

def proc_other (src = False) :
    proc_new_other ()

def prog_source () :
    proc_variant ("Source", source_config, source_subdir, src=True)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   read_options ()
   initProductFiles ()
   if opt.install :
      proc_install ()
      exit
   if opt.mini :
      proc_mini ()
      exit
   if opt.updates :
      proc_updates ()
      exit
   if opt.product :
      proc_product ()
      exit
   if opt.combine :
      proc_combine ()
      exit
   if opt.iso :
      proc_iso ()
      exit
   if opt.all :
      proc_mini ()
      proc_prog ()
      proc_updates ()
      proc_product ()
      proc_combine ()
      proc_iso ()
      exit
   if opt.prog :
      proc_prog ()
      exit
   if opt.prog_src :
      proc_prog (src=True)
      exit
   if opt.other :
      proc_other ()
      exit
   if opt.large :
      proc_large ()
      exit
   if opt.large_src :
      proc_large (src=True)
      exit
   if opt.source :
      proc_source ()
      exit
   if opt.small :
      proc_small ()
      exit
   if opt.small_src :
      proc_small (src=True)
      exit
   if opt.tiny :
      proc_tiny ()
      exit
   if opt.tiny_src :
      proc_tiny (src=True)
      exit
   if opt.qemu :
      proc_qemu ()
      exit
   if opt.copy :
      proc_copy ()
      exit
   if opt.delta :
      proc_delta ()
      exit

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
