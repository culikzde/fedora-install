
%packages

@admin-tools
@base-x
@core
@dial-up
@fonts
@guest-desktop-agents
@hardware-support
@input-methods
# @libreoffice
@multimedia
@networkmanager-submodules
@printing
@standard
# @workstation-product

@firefox
mc

@mate-desktop
@mate-applications

gcc
gcc-c++

ccache

automake
libtool

binutils-devel
libtool-ltdl-devel

pkgconfig

gtk2-devel
gtk2-devel-docs

gtk3-devel
gobject-introspection-devel
gtk3-devel-docs

qt-devel
# qt5-devel

# PyQt4

python3-qt5
python3-qt5-webkit
python3-qt5-webengine

qt-qdbusviewer
qt5-qdbusviewer


kate
kompare
kdevelop
kdevelop-php
kdevelop-python

qt-creator

cpio
genisoimage
nmap-ncat
lua
openssh
openssh-clients
python3-rangehttpserver

python-unversioned-command
wpa_supplicant

# pungi requires
# slick-greeter-cinnamon

-kde-i18n-*
-kde-l10n-*

-man-pages-*

-qt5-qt3d-examples

kernel*

@anaconda-tools

%end
